# How to use this overlay

## With layman

1. Emerge layman:
   ```
   emerge -av layman
   ```

2. Run the following:
   ```
   layman -o https://gitlab.com/rypervenche/ryper-gentoo/raw/master/repositories.xml -f -a ryper-gentoo
   ```
